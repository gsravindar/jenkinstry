package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week4.day2.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement eleCRMLink;
	
	
	
	
	public MyHomePage clickEleCRMLink() {
		click(eleCRMLink);
		return new MyHomePage();
		
		
		
	}

}
