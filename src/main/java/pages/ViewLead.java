package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week4.day2.ProjectMethods;

public class ViewLead extends ProjectMethods {
	
	
	
	
	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.ID, using = "viewLead_firstName_sp") WebElement eleFirstName;
		
	
	public ViewLead verifyFirstName() {
		
		verifyExactText(eleFirstName, "Raj");
		return this;
		
		}
		
		
		
		
		
	}

