package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CreateLeads extends ProjectMethods {
	//@Test(invocationCount=2, timeOut = 3000,enabled = false)
	
	public String dataSheetName;
	
	@Test (dataProvider = "qa")
	
	public void CrLeads(String cname, String fname, String lname)
	{
		
		WebElement eleCrm = locateElement("text", "CRM/SFA");
		click(eleCrm);
		WebElement eleCreate = locateElement("text", "Create Lead");
		click(eleCreate);
		WebElement compNme = locateElement("createLeadForm_companyName");
		type(compNme, cname);
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, fname);
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, lname);
		verifyExactText(lastName, lname);
		WebElement dropd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(dropd, 2);
		WebElement createButton = locateElement("class", "smallSubmit");
		click(createButton);

	}
	@DataProvider (name = "qa", indices = {2})
	public Object[][] fetchData() throws IOException
	{
		Object[][] data = ReadExcel.readExcel(dataSheetName);
		return data;
		/*String [][] dat = new String [2][3] ;
		dat [0][0] = "IBM"; 
		dat [0][1] = "RAVI"; 
		dat [0][2] = "S"; 
		
		dat [1][0] = "IBM"; 
		dat [1][1] = "KARTHI"; 
		dat [1][2] = "G"; 
		return dat;*/
		
		//return DataInputProvider.getSheet(datashheetName);
	}
	
	}
