package week4.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	//@Test
	public static Object [][] readExcel(String dataSheetName) throws IOException
	{
		
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int RowNum = sheet.getLastRowNum();
		int CellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data  = new Object[RowNum][CellNum];
		for (int j = 1; j <= RowNum; j++) {
			XSSFRow row = sheet.getRow(j);
		
		for (int i = 0; i < CellNum; i++) {
			XSSFCell cell = row.getCell(i);
			
			try {
				String val = cell.getStringCellValue();
				System.out.println(val);
				data[j-1][i] = val;
			} catch (NullPointerException e) {
				System.out.println("");
				
			}
		}
	}
		
	wb.close();	
	return data;
}
}

