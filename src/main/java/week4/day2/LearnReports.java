package week4.day2;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports extends SeMethods {

	@Test
public void createLeads () throws IOException
{
	ExtentHtmlReporter html = new ExtentHtmlReporter("./project/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
extent.attachReporter(html);	

ExtentTest logger = extent.createTest("TC001", "CREATE LEAD");
logger.assignAuthor("Ravi");
logger.assignCategory("Functional");
logger.log(Status.PASS, "Logged in successfully", MediaEntityBuilder.createScreenCaptureFromPath("./Snaps/img1.png").build());
extent.flush();
}
	}


