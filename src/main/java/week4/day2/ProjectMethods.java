package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods{
	
	public String dataSheetName;

	//public static void main(String[] args) {
	// TODO Auto-generated method stub
	//@Parameters({"url", "uname", "pwd"})
	
	
	@BeforeMethod(groups= {"any"})
	public void login( )
	{
		startApp("chrome","http://leaftaps.com/opentaps");
		/*WebElement eleLogin = locateElement("id", "username");
		type(eleLogin, uname);
		WebElement passwd = locateElement("password");
		type(passwd, pwd);
		WebElement choose = locateElement("class", "decorativeSubmit");
		click(choose);*/
	}
	@AfterMethod(groups= {"any"})
	public void close()
	{
		closeBrowser();
	}
	
	
	@DataProvider(name="qa")
	public Object[][] getData() throws IOException{
		Object[][] data = ReadExcel.readExcel(dataSheetName);
		return data;
		
		//return DataInputProvider.getSheet(datasheetName);
	}
	
}


