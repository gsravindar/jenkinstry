package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameAlert2 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\eclipse-workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		
		WebElement ex = driver.findElementByXPath("//button[text()='Try it']");
		driver.switchTo().frame(ex);
		ex.click();
		
		
		/*
		driver.switchTo().alert().sendKeys("Ravi");
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		
		
		String text = driver.findElementByXPath("//p[@id='demo']").getText();
		if(text.contains("Ravi")) {
			System.out.println("Verified");
		}else System.out.println("No text");
		
		*/
		
	}

}
